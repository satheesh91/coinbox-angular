import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  showForm = false;

  constructor(
    private api: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.api.getAuthToken.subscribe((res: any) => {
      this.showForm = true;
      this.api.setAuthToken(res.access_token);
    });

    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;

    if (this.form.invalid) return;

    this.loading = true;

    let { username, password } = this.form.value;

    this.api.signinUser(username, password).subscribe(
      (res: any) => {
        console.log(res);
        this.api.setToken(res.access_token);
        this.router.navigate(['/dashboard']);
      },
      (err) => {
        console.log(err);
        this.loading = false;
        this.notifier.show({
          type: 'error',
          message: 'Email or Password not match with our records',
        });
      }
    );
  }
}
