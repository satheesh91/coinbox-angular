import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment.prod';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  token: string;
  userdata: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private router: Router, private http: HttpClient) {
    this.token = localStorage.token;
  }

  setToken(token) {
    localStorage.token = token;
    this.token = localStorage.token;
  }

  get getAuthToken() {
    return this.http.get(`${environment.apiBase}/authtoken`);
  }

  setAuthToken(token) {
    localStorage.authToken = token;
  }

  signinUser(username: string, password: string) {
    return this.http.post(
      `${environment.apiBase}/ausers/login`,
      {
        userName: username,
        password,
      },
      {
        headers: new HttpHeaders({
          Authorization: localStorage.authToken,
        }),
      }
    );
  }

  logout() {
    this.token = null;
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }

  getToken() {
    return this.token;
  }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token
    return this.token ? true : false;
  }

  get currentUserData() {
    return JSON.parse(localStorage.getItem('userdata'));
  }

  get currentUserID() {
    let userData = JSON.parse(localStorage.getItem('userdata'));
    return userData.id;
  }

  get currentUserRole() {
    let userData = JSON.parse(localStorage.getItem('userdata'));
    return userData.role_id;
  }

  get getProfileObs() {
    return this.userdata.asObservable();
  }

  setProfileObs(user_id) {
    this.http
      .get(`${environment.apiBase}/users/profile/${user_id}`, {
        headers: new HttpHeaders({
          Authorization: localStorage.authToken,
        }),
      })
      .subscribe(
        (data) => {
          this.userdata.next(data);
        },
        (err) => console.log(err)
      );
  }
}
