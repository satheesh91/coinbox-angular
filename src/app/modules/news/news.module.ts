import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared.module';
import { NewsRoutingModule } from './news-routing.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

@NgModule({
  declarations: [ListComponent, CreateComponent, EditComponent],
  imports: [CommonModule, SharedModule, NewsRoutingModule],
})
export class NewsModule {}
