import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  constructor(private http: HttpClient) {}

  get list() {
    return this.http.get(`${environment.apiBase}/news/list?limit=999&skip=0`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  create(data) {
    return this.http.post(`${environment.apiBase}/news/add`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  get(id) {
    return this.http.get(`${environment.apiBase}/news/${id}`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  update(id, data) {
    return this.http.put(`${environment.apiBase}/news/update/${id}`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }
}
