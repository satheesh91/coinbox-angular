import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsService } from '../news.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  base64textString: any = '';
  base64Image: any = '';

  constructor(
    private fb: FormBuilder,
    private api: NewsService,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      title: ['', Validators.required],
      time: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  get f() {
    return this.form.controls;
  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(e) {
    this.base64textString = 'data:image/png;base64,' + btoa(e.target.result);
    this.base64Image = btoa(e.target.result);
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;

    let data = {
      title: this.form.value.title,
      time: this.form.value.time,
      images: this.base64textString,
      description: this.form.value.description,
      isActive: true,
    };

    this.api.create(data).subscribe(
      (res) => {
        console.log(res);
        this.form.reset();
        this.submitted = false;
        this.notifier.show({
          type: 'success',
          message: 'News is created',
        });
      },
      (err) => {
        this.notifier.show({
          type: 'error',
          message: JSON.stringify(err),
        });
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}
