import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsService } from '../news.service';
import { NotifierService } from 'angular-notifier';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  fetching = false;

  constructor(
    private fb: FormBuilder,
    private api: NewsService,
    private notifier: NotifierService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      title: ['', Validators.required],
      time: ['', Validators.required],
      description: ['', Validators.required],
      isActive: [true, Validators.required],
    });
    this.fetch();
  }

  fetch() {
    this.fetching = true;
    this.api.get(this.route.snapshot.params.id).subscribe(
      (res) => {
        this.fetching = false;
        this.form.patchValue(res);
      },
      (err) => console.log(err)
    );
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;

    let data = {
      title: this.form.value.title,
      time: this.form.value.time,
      images: '1.jpg',
      description: this.form.value.description,
      isActive: this.form.value.isActive == 'true',
    };

    this.api.update(this.route.snapshot.params.id, data).subscribe(
      (res) => {
        console.log(res);
        this.submitted = false;
        this.notifier.show({
          type: 'success',
          message: 'News is updated',
        });
      },
      (err) => {
        this.notifier.show({
          type: 'error',
          message: JSON.stringify(err),
        });
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}
