import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    pathMatch: 'full',
    data: { title: 'News' },
  },
  {
    path: 'create',
    component: CreateComponent,
    pathMatch: 'full',
    data: { title: 'Create News' },
  },
  {
    path: ':id/edit',
    component: EditComponent,
    pathMatch: 'full',
    data: { title: 'Edit News' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsRoutingModule {}
