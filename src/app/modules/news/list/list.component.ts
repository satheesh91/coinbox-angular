import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  list: any = [];
  loading = false;

  @BlockUI('loading') blockUI: NgBlockUI;

  constructor(private api: NewsService, private notify: NotifierService) {}

  ngOnInit(): void {
    this.fetch();
  }

  fetch() {
    this.blockUI.start('Loading...');
    this.api.list.subscribe(
      (res: any) => {
        this.list = res.details;
      },
      (err) => console.log(err),
      () => this.blockUI.stop()
    );
  }
}
