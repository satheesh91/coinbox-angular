import { Component, OnInit, ViewChild } from '@angular/core';
import { WithdrawRequestsService } from '../withdraw-requests.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { NotifierService } from 'angular-notifier';
import {
  BsModalService,
  BsModalRef,
  ModalDirective,
} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  @BlockUI('loading') blockUI: NgBlockUI;
  @ViewChild(ModalDirective) modal: ModalDirective;

  page = 1;
  limit = 10;
  total = 0;
  list: any = [];
  types: any = 'opened';
  searchText: any = '';
  loading = false;
  otp: any = '';
  request_id: any = '';

  constructor(
    private api: WithdrawRequestsService,
    private notify: NotifierService
  ) {}

  ngOnInit(): void {
    this.fetch();
  }

  fetch() {
    this.blockUI.start('Loading...');
    this.api.list(this.page, this.limit, this.types, this.searchText).subscribe(
      (res: any) => {
        this.list = res.details.listing;
        this.total = res.details.pagination;
        console.log(this.list);
      },
      (err) => console.log(err),
      () => this.blockUI.stop()
    );
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.fetch();
  }

  search(): void {
    this.page = 1;
    this.fetch();
  }

  sendOTP(request_id) {
    this.request_id = request_id;
    this.blockUI.start('Loading...');
    this.api.sendTransferOTP.subscribe(
      (res: any) => {
        this.blockUI.stop();
        this.notify.show({ type: 'success', message: res.message });
        this.modal.show();
      },
      (err) => {
        console.log(err);
        this.notify.show({ type: 'error', message: JSON.stringify(err) });
      }
    );
  }

  verify() {
    if (this.otp != '') {
      this.loading = true;
      this.api.transferRequest(this.request_id, parseInt(this.otp)).subscribe(
        (res: any) => {
          this.notify.show({ type: 'success', message: res.message });
          this.modal.hide();
          this.loading = false;
          this.fetch();
        },
        (err: any) => {
          console.log(err);
          this.notify.show({ type: 'error', message: err.error.message });
          this.loading = false;
        }
      );
    }
  }
}
