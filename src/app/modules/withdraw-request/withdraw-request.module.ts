import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared.module';
import { ListComponent } from './list/list.component';
import { TransferRequestComponent } from './transfer-request/transfer-request.component';
import { WithdrawRequestsRoutingModule } from './withdraw-requests-routing.module';
import { AllRequestsComponent } from './all-requests/all-requests.component';

@NgModule({
  declarations: [ListComponent, TransferRequestComponent, AllRequestsComponent],
  imports: [CommonModule, SharedModule, WithdrawRequestsRoutingModule],
})
export class WithdrawRequestModule {}
