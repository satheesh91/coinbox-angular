import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class WithdrawRequestsService {
  constructor(private http: HttpClient) {}

  list(page, limit, type = 'all', search = '') {
    return this.http.get(
      `${environment.apiBase}/userwallet/withdrawtransfer?search=${search}&type=${type}&limit=${limit}&page=${page}`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  get sendTransferOTP() {
    return this.http.post(
      `${environment.apiBase}/userwallet/sendTransferOtp`,
      {},
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  transferRequest(id, otp) {
    return this.http.post(
      `${environment.apiBase}/userwallet/transferrequest`,
      { request_id: id, otp: otp },
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }
}
