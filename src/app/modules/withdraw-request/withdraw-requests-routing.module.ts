import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { TransferRequestComponent } from './transfer-request/transfer-request.component';
import { AllRequestsComponent } from './all-requests/all-requests.component';

const routes: Routes = [
  {
    path: 'withdraw',
    component: ListComponent,
    pathMatch: 'full',
    data: { title: 'Withdraw Requests' },
  },
  {
    path: 'all',
    component: AllRequestsComponent,
    pathMatch: 'full',
    data: { title: 'Withdraw Requests' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WithdrawRequestsRoutingModule {}
