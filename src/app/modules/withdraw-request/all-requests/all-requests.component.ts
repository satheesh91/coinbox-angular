import { Component, OnInit } from '@angular/core';
import { WithdrawRequestsService } from '../withdraw-requests.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-all-requests',
  templateUrl: './all-requests.component.html',
  styleUrls: ['./all-requests.component.css'],
})
export class AllRequestsComponent implements OnInit {
  @BlockUI('loading') blockUI: NgBlockUI;

  page = 1;
  limit = 10;
  total = 0;
  list: any = [];
  types: any = 'all';
  searchText: any = '';
  loading = false;

  constructor(private api: WithdrawRequestsService) {}

  ngOnInit(): void {
    this.fetch();
  }

  fetch() {
    this.blockUI.start('Loading...');
    this.api.list(this.page, this.limit, this.types, this.searchText).subscribe(
      (res: any) => {
        this.list = res.details.listing;
        this.total = res.details.pagination;
        console.log(this.list);
      },
      (err) => console.log(err),
      () => this.blockUI.stop()
    );
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.fetch();
  }

  search(): void {
    this.page = 1;
    this.fetch();
  }
}
