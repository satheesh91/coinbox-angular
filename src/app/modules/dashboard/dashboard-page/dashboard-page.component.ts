import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { ChartComponent } from 'ng-apexcharts';
import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css'],
})
export class DashboardPageComponent implements OnInit {
  data: any;
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  loading = true;

  constructor(private api: DashboardService) {}

  ngOnInit(): void {
    this.api.get.subscribe(
      (res) => {
        this.data = res;
        this.loading = false;

        this.chartOptions = {
          series: [
            this.data.details.planCount.copper,
            this.data.details.planCount.zinc,
            this.data.details.planCount.earth,
          ],
          chart: {
            type: 'donut',
            height: 230,
          },
          labels: [
            `Copper - (${this.data.details.planCount.copper})`,
            `Zinc - (${this.data.details.planCount.zinc})`,
            `Earth - (${this.data.details.planCount.earth})`,
          ],
          responsive: [
            {
              breakpoint: 480,
              options: {
                legend: false,
              },
            },
          ],
        };
      },
      (err) => console.log(err)
    );
  }
}
