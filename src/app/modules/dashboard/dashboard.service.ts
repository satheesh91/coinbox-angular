import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  get get() {
    return this.http.get(`${environment.apiBase}/adashboard`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }
}
