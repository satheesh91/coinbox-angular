import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class PaymentsService {
  constructor(private http: HttpClient) {}

  searchHistories(user_id, page, limit) {
    return this.http.get(
      `${environment.apiBase}/wallethistory/paymentHistory/?user_id=${user_id}&page=${page}&limit=${limit}`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }
}
