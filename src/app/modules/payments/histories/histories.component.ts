import { Component, OnInit } from '@angular/core';
import { PaymentsService } from '../payments.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../users/users.service';

@Component({
  selector: 'app-histories',
  templateUrl: './histories.component.html',
  styleUrls: ['./histories.component.css'],
})
export class HistoriesComponent implements OnInit {
  pageLoading = false;
  data: any = [];
  page = 1;
  limit = 10;
  total = 0;
  @BlockUI('loading') blockUI: NgBlockUI;
  submitted = false;
  searching = false;
  form: FormGroup;

  constructor(
    private api: PaymentsService,
    private fb: FormBuilder,
    private userApi: UsersService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      user_id: [null, Validators.required],
    });
  }

  fetch() {
    this.blockUI.start('Loading...');
    this.searching = true;
    this.userApi.getUserInfo(this.form.value.user_id).subscribe(
      (user: any) => {
        this.api
          .searchHistories(user.details.hash, this.page, this.limit)
          .subscribe(
            (res: any) => {
              this.data = res;
              this.searching = false;
              this.total =
                res.details.totalCount != undefined
                  ? res.details.totalCount
                  : 0;
              // console.log(this.total);
            },
            (err) => {
              console.log(err);
              this.searching = false;
              this.blockUI.stop();
            },
            () => this.blockUI.stop()
          );
      },
      (err) => {
        console.log(err);
        this.searching = false;
        this.blockUI.stop();
      }
    );
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) return;

    this.fetch();
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.fetch();
  }
}
