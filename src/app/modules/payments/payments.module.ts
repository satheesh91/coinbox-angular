import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsRoutingModule } from './payments-routing.module';
import { SharedModule } from '../../../app/shared.module';
import { HistoriesComponent } from './histories/histories.component';

@NgModule({
  declarations: [HistoriesComponent],
  imports: [CommonModule, PaymentsRoutingModule, SharedModule],
})
export class PaymentsModule {}
