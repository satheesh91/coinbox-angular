import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { PanelComponent } from './panel/panel.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchProfileComponent } from './search-profile/search-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    PanelComponent,
    ProfileComponent,
    SearchProfileComponent,
    ChangePasswordComponent,
  ],
  imports: [CommonModule, SharedModule, UsersRoutingModule],
})
export class UsersModule {}
