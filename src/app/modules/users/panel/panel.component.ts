import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css'],
})
export class PanelComponent implements OnInit {
  activation_user: any;
  referral_id: any;

  m_referral_id: any;
  m_activation_user: any;

  c_referral_id: any;
  c_activation_user: any;
  c_coins: any;

  success: any = {
    activation: false,
    m_activation: false,
    c_activation: false,
  };

  constructor(private api: UsersService, private notify: NotifierService) {}

  ngOnInit(): void {}

  getUserInfo() {
    if (this.referral_id.length >= 3) {
      this.api.getUserInfo(this.referral_id).subscribe(
        (res) => {
          this.activation_user = res;
          this.success.activation = true;
        },
        (err) => console.log(err)
      );
    }
  }

  activate() {
    if (this.referral_id != '' && this.referral_id != undefined) {
      this.api.changeStatus(this.referral_id, 'activate').subscribe(
        (res) => {
          this.getUserInfo();
          this.notify.show({ type: 'success', message: 'User is activated' });
        },
        (err) => console.log(err)
      );
    } else {
      alert('Please enter the valid user id');
    }
  }

  deactivate() {
    if (this.referral_id != '' && this.referral_id != undefined) {
      this.api.changeStatus(this.referral_id, 'deactivate').subscribe(
        (res) => {
          this.getUserInfo();
          this.notify.show({ type: 'error', message: 'User is deactivated' });
        },
        (err) => console.log(err)
      );
    } else {
      alert('Please enter the valid user id');
    }
  }

  getMembershipUserInfo() {
    if (this.m_referral_id.length >= 3) {
      this.api.getUserInfo(this.m_referral_id).subscribe(
        (res) => {
          this.m_activation_user = res;
          this.success.m_activation = true;
        },
        (err) => console.log(err)
      );
    }
  }

  changePlan(plan_id) {
    if (
      this.m_activation_user.details.hash != undefined &&
      this.m_activation_user.details.hash != ''
    ) {
      this.api
        .changePlan(this.m_activation_user.details.hash, plan_id)
        .subscribe(
          (res) => {
            this.getMembershipUserInfo();
            this.notify.show({
              type: 'success',
              message: 'Membership plan is changed',
            });
          },
          (err: any) => {
            console.log(err);
            this.notify.show({ type: 'error', message: err.error.message });
          }
        );
    }
  }

  getCoinUserInfo() {
    if (this.c_referral_id.length >= 3) {
      this.api.getUserInfo(this.c_referral_id).subscribe(
        (res) => {
          this.c_activation_user = res;
          this.success.c_activation = true;
        },
        (err) => console.log(err)
      );
    }
  }

  addCoins() {
    if (
      this.c_referral_id != '' &&
      this.c_referral_id != undefined &&
      this.c_coins != '' &&
      this.c_coins != undefined
    ) {
      this.api.addCoins(this.c_referral_id, this.c_coins).subscribe(
        (res: any) => {
          this.getCoinUserInfo();
          this.notify.show({ type: 'success', message: res.message });
        },
        (err: any) => {
          console.log(err);
          this.notify.show({ type: 'error', message: err.message });
        }
      );
    }
  }
}
