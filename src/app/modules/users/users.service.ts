import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  list(page, limit, types = 'all', search = '') {
    return this.http.get(
      `${environment.apiBase}/users/allusers/${encodeURIComponent(
        types
      )}/${page}/${limit}/?search=${search}`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  add(body) {
    return this.http.post(`${environment.apiBase}/users/add`, body, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  getUserInfo(id) {
    return this.http.get(`${environment.apiBase}/users/user_name/${id}`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  changeStatus(id, status) {
    return this.http.put(
      `${environment.apiBase}/users/status/${id}/${status}`,
      {},
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  changePlan(id, plan_id) {
    let slug = plan_id == 'SUBCOP' ? 'plan/new' : 'plan/upgrade';

    return this.http.post(
      `${environment.apiBase}/${slug}`,
      {
        user_id: id,
        plan_id: plan_id,
        wallet: false,
      },
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  addCoins(user_id, coin) {
    return this.http.post(
      `${environment.apiBase}/coinvalue/cointouser`,
      {
        user_id: user_id,
        coin: parseInt(coin),
      },
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  getProfile(hash_id) {
    return this.http.get(`${environment.apiBase}/users/profile/${hash_id}`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  updateProfile(data) {
    return this.http.post(`${environment.apiBase}/users/profile`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  getWalletInfo(hash) {
    return this.http.get(`${environment.apiBase}/userwallet/info/${hash}`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  changePassword(data) {
    return this.http.post(
      `${environment.apiBase}/ausers/change_password`,
      data,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }
}
