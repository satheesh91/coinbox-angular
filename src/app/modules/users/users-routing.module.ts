import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { PanelComponent } from './panel/panel.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchProfileComponent } from './search-profile/search-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    pathMatch: 'full',
    data: { title: 'Users' },
  },
  {
    path: 'create',
    component: CreateComponent,
    pathMatch: 'full',
    data: { title: 'Create User' },
  },
  {
    path: 'panel',
    component: PanelComponent,
    pathMatch: 'full',
    data: { title: 'User Panel' },
  },
  {
    path: ':id/edit',
    component: ProfileComponent,
    pathMatch: 'full',
    data: { title: 'Edit User' },
  },
  {
    path: 'search-profile',
    component: SearchProfileComponent,
    pathMatch: 'full',
    data: { title: 'Search Profile' },
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    pathMatch: 'full',
    data: { title: 'Change Password' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
