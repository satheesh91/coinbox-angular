import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;

  pageLoading = false;

  constructor(
    private api: UsersService,
    private fb: FormBuilder,
    private notify: NotifierService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      userName: ['', Validators.required],
      newPassword: ['', Validators.required],
      oldPassword: ['', Validators.required],
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;

    this.loading = true;

    this.api.changePassword(this.form.value).subscribe(
      (res) => {
        console.log(res);
        this.submitted = false;
        this.form.reset();
        this.notify.show({
          type: 'success',
          message: 'Password is changed',
        });
      },
      (err) => {
        console.log(err);
        this.notify.show({
          type: 'error',
          message: err.error.message,
        });
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}
