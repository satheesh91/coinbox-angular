import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { NotifierService } from 'angular-notifier';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private api: UsersService,
    private notifier: NotifierService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      user_id: '',
      name: ['', Validators.required],
      dob: '',
      email: ['', [Validators.email, Validators.required]],
      mobileNo: ['', Validators.required],
      countryMobileCode: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      pincode: ['', Validators.required],
    });

    this.fetchData();
  }

  fetchData() {
    this.form.patchValue({ user_id: this.route.snapshot.params.id });
    this.api.getProfile(this.route.snapshot.params.id).subscribe(
      (res: any) => {
        this.form.patchValue(res.details);
      },
      (err) => console.log(err)
    );
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.f);
    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;
    this.form.value.countryMobileCode = this.form.value.countryMobileCode.toString();
    this.form.value.mobileNo = this.form.value.mobileNo.toString();

    this.api.updateProfile(this.form.value).subscribe(
      (res) => {
        console.log(res);
        this.fetchData();
        this.notifier.show({
          type: 'success',
          message: 'User profile is updated',
        });
      },
      (err) => {
        this.notifier.show({
          type: 'error',
          message: JSON.stringify(err),
        });
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}
