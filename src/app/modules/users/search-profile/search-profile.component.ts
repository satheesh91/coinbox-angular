import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-search-profile',
  templateUrl: './search-profile.component.html',
  styleUrls: ['./search-profile.component.css'],
})
export class SearchProfileComponent implements OnInit {
  data: any = [];
  user_id: any;
  loading = false;

  constructor(private api: UsersService) {}

  ngOnInit(): void {}

  search() {
    if (this.user_id) {
      this.loading = true;
      this.api.getUserInfo(this.user_id).subscribe((res) => {
        this.data = res;
        this.api.getProfile(this.data.details.hash).subscribe((res2) => {
          this.data['profile'] = res2;
          this.api.getWalletInfo(this.data.details.hash).subscribe((res3) => {
            this.data['wallet'] = res3;
          });
        });
        this.loading = false;
      });
    }
  }
}
