import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  user_id: any = null;
  form: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private api: UsersService,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
      mobileNo: ['', Validators.required],
      countryMobileCode: ['', Validators.required],
      referredBy: [''],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      pincode: ['', Validators.required],
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.user_id = null;
    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;
    this.form.value.countryMobileCode = this.form.value.countryMobileCode.toString();
    this.form.value.mobileNo = this.form.value.mobileNo.toString();
    this.api.add(this.form.value).subscribe(
      (res: any) => {
        console.log(res);
        this.form.reset();
        this.submitted = false;
        this.notifier.show({
          type: 'success',
          message: 'User is created: ' + res.details.user_id,
        });
        this.user_id = res.details.user_id;
      },
      (err) => {
        console.log(err);
        this.notifier.show({
          type: 'error',
          message: err.error.message,
        });
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}
