import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  @BlockUI('loading') blockUI: NgBlockUI;

  page = 1;
  limit = 10;
  total = 0;
  users: any = [];
  types: any = 'all';
  searchText: any = '';
  loading = true;
  statuses: any = [];
  selectedStatus: any = ['all'];

  constructor(private api: UsersService) {}

  ngOnInit(): void {
    this.fetch();
    this.statuses = [
      { id: 'all', label: 'All' },
      { id: 'active', label: 'Active' },
      { id: 'inactive', label: 'In-Active' },
      { id: 'deactivate', label: 'Deactivate' },
      { id: 'copper', label: 'Copper' },
      { id: 'zinc', label: 'Zinc' },
      { id: 'earth', label: 'Earth' },
      { id: 'golden', label: 'Golden' },
      { id: 'platinum', label: 'Platinum' },
      { id: 'streamline', label: 'Streamline' },
      { id: 'treasure', label: 'Treasure' },
    ];
  }

  fetch() {
    this.blockUI.start('Loading...');
    this.types = this.selectedStatus.length
      ? this.selectedStatus.join(',')
      : '';
    this.api.list(this.page, this.limit, this.types, this.searchText).subscribe(
      (res: any) => {
        this.users = res.details.listing;
        this.total = res.details.totalCounts;
      },
      (err) => console.log(err),
      () => this.blockUI.stop()
    );
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.fetch();
  }

  search(): void {
    this.page = 1;
    this.fetch();
  }

  filterByStatus(event) {
    this.fetch();
  }
}
