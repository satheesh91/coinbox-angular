import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubsciptionFilesComponent } from './subsciption-files/subsciption-files.component';

const routes: Routes = [
  {
    path: 'upload-subscription-file',
    component: SubsciptionFilesComponent,
    data: { title: 'Upload Files' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FilesRoutingModule {}
