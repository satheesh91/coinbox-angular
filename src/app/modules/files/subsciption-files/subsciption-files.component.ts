import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FilesService } from '../files.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-subsciption-files',
  templateUrl: './subsciption-files.component.html',
  styleUrls: ['./subsciption-files.component.css'],
})
export class SubsciptionFilesComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  base64textString: any;

  constructor(
    private fb: FormBuilder,
    private api: FilesService,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      location: ['COPPER', Validators.required],
      image: [null, Validators.required],
      pdf: [null, Validators.required],
    });
  }

  get f() {
    return this.form.controls;
  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  handleReaderLoaded(e) {
    this.base64textString = 'data:image/png;base64,' + btoa(e.target.result);
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;

    let image: any = document.getElementById('upload-image');
    let pdf: any = document.getElementById('upload-pdf');

    let formData: FormData = new FormData();
    formData.append('files', image.files[0]);
    formData.append('files', pdf.files[0]);

    this.api
      .uploadSubscriptionFile(this.form.value.location, formData)
      .subscribe(
        (res) => {
          this.notifier.show({
            type: 'success',
            message: 'Files uploaded successfully',
          });
          this.loading = false;
          this.form.reset();
          this.form.patchValue({ location: 'COPPER' });
          this.submitted = false;
          this.base64textString = '';
        },
        (err) => {
          console.log(err);
          this.notifier.show({ type: 'error', message: err.error });
          this.loading = false;
        }
      );
  }
}
