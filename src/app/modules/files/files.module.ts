import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared.module';

import { FilesRoutingModule } from './files-routing.module';
import { SubsciptionFilesComponent } from './subsciption-files/subsciption-files.component';

@NgModule({
  declarations: [SubsciptionFilesComponent],
  imports: [CommonModule, FilesRoutingModule, SharedModule],
})
export class FilesModule {}
