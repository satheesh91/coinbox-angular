import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class UserValuesService {
  constructor(private http: HttpClient) {}

  get getAllValues() {
    let subscriptions = this.http.get(
      `${environment.apiBase}/subscription/all`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
    let configData = this.http.get(`${environment.apiBase}/configdata`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
    let coinHistory = this.http.get(
      `${environment.apiBase}/coinvalue/history`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
    let treasure = this.http.get(`${environment.apiBase}/treasure`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
    let lastTreasureWinner = this.http.get(
      `${environment.apiBase}/treasure/last_treasure_winner`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );

    return {
      subscriptions,
      configData,
      coinHistory,
      treasure,
      lastTreasureWinner,
    };
  }

  setCoinValue(data) {
    return this.http.post(`${environment.apiBase}/coinvalue/add`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  updateConfigData(data) {
    return this.http.post(`${environment.apiBase}/configdata/update`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }
}
