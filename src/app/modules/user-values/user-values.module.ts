import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserValuesRoutingModule } from './user-values-routing.module';
import { ViewValuesComponent } from './view-values/view-values.component';
import { SharedModule } from '../../../app/shared.module';

@NgModule({
  declarations: [ViewValuesComponent],
  imports: [CommonModule, UserValuesRoutingModule, SharedModule],
})
export class UserValuesModule {}
