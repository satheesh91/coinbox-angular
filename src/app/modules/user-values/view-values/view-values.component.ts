import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { UserValuesService } from '../user-values.service';

@Component({
  selector: 'app-view-values',
  templateUrl: './view-values.component.html',
  styleUrls: ['./view-values.component.css'],
})
export class ViewValuesComponent implements OnInit {
  data: any = {};
  copper: any;
  zinc: any;
  earth: any;
  golden: any = null;
  platinum: any = null;

  form: FormGroup;
  submitted = false;
  loading = false;
  edit = false;

  pageLoading = true;

  constructor(
    private api: UserValuesService,
    private fb: FormBuilder,
    private notify: NotifierService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [''],
      streamLineLimit: ['', Validators.required],
      streamLineBonus: ['', Validators.required],
      adminCharges: ['', Validators.required],
      value: ['', Validators.required],
    });
    this.api.getAllValues.subscriptions.subscribe(
      (res: any) => {
        this.data.subscriptions = res;
        this.copper = res.length
          ? res.filter((item) => item.name.toLowerCase() == 'copper').shift()
          : {};
        this.zinc = res.length
          ? res.filter((item) => item.name.toLowerCase() == 'zinc').shift()
          : {};
        this.earth = res.length
          ? res.filter((item) => item.name.toLowerCase() == 'earth').shift()
          : {};

        this.golden = res.length
          ? res.filter((item) => item.name.toLowerCase() == 'golden').shift()
          : {};

        this.platinum = res.length
          ? res.filter((item) => item.name.toLowerCase() == 'platinum').shift()
          : {};

        this.pageLoading = false;
      },
      (err) => console.log(err)
    );

    this.fetchEditFormData();

    this.api.getAllValues.treasure.subscribe(
      (res) => (this.data.treasure = res),
      (err) => console.log(err)
    );
    this.api.getAllValues.lastTreasureWinner.subscribe(
      (res) => (this.data.lastTreasureWinner = res),
      (err) => console.log(err)
    );
  }

  fetchEditFormData() {
    this.api.getAllValues.configData.subscribe(
      (res) => {
        this.data.configData = res;
        this.form.patchValue({
          id: this.data.configData.details._id,
          streamLineLimit: this.data.configData.details.streamLineLimit,
          streamLineBonus: this.data.configData.details.streamLineBonus,
          adminCharges: this.data.configData.details.adminCharges,
        });
      },
      (err) => console.log(err)
    );
    this.api.getAllValues.coinHistory.subscribe(
      (res) => {
        this.data.coinHistory = res;
        this.form.patchValue({
          value: this.data.coinHistory.details.coinHistory[
            this.data.coinHistory.details.coinHistory.length - 1
          ].value,
        });
      },
      (err) => console.log(err)
    );
  }

  get f() {
    return this.form.controls;
  }

  editConfig() {
    this.edit = !this.edit;
  }

  update() {
    this.submitted = true;
    if (this.form.invalid) return;

    this.loading = true;

    this.api.setCoinValue(this.form.value).subscribe(
      (res) => {
        this.notify.show({
          type: 'success',
          message: 'Coin value has been updated',
        });
      },
      (err) => {
        console.log(err);
        this.notify.show({
          type: 'error',
          message: JSON.stringify(err.error),
        });
        this.fetchEditFormData();
        this.loading = false;
      }
    );

    this.api.updateConfigData(this.form.value).subscribe(
      (res) => {
        this.notify.show({
          type: 'success',
          message: 'Configuration has been updated',
        });
        this.fetchEditFormData();
      },
      (err) => {
        console.log(err);
        this.notify.show({
          type: 'error',
          message: JSON.stringify(err.error),
        });
        this.fetchEditFormData();
        this.loading = false;
      },
      () => (this.loading = false)
    );
  }
}
