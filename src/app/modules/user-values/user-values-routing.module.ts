import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewValuesComponent } from './view-values/view-values.component';

const routes: Routes = [
  {
    path: '',
    component: ViewValuesComponent,
    pathMatch: 'full',
    data: { title: 'My Values' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserValuesRoutingModule {}
