import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchDownlinesComponent } from './search-downlines/search-downlines.component';

const routes: Routes = [
  {
    path: 'search',
    component: SearchDownlinesComponent,
    data: { title: 'Downlines' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DownlinesRoutingModule {}
