import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DownlinesRoutingModule } from './downlines-routing.module';
import { SearchDownlinesComponent } from './search-downlines/search-downlines.component';
import { SharedModule } from '../../../app/shared.module';

@NgModule({
  declarations: [SearchDownlinesComponent],
  imports: [CommonModule, DownlinesRoutingModule, SharedModule],
})
export class DownlinesModule {}
