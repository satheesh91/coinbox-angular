import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class DownlinesService {
  constructor(private http: HttpClient) {}

  searchDownlines(user_id, level, page, limit) {
    return this.http.get(
      `${environment.apiBase}/userlevel/${user_id}/${level}/${page}/${limit}`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }
}
