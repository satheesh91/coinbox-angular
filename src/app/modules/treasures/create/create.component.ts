import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TreasuresService } from '../treasures.service';
import { NotifierService } from 'angular-notifier';
import { UsersService } from '../../users/users.service';
import * as moment from 'moment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  users: any = [];

  constructor(
    private fb: FormBuilder,
    private api: TreasuresService,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      announceDate: ['', Validators.required],
      announceTime: ['', Validators.required],
      cpv: ['', Validators.required],
      user_id: ['', Validators.required],
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;
    this.loading = true;

    this.form.value.announceDate = moment(
      this.form.value.announceDate + ' ' + this.form.value.announceTime,
      'YYYY-MM-DD HH:mm:ss'
    ).format('YYYY-MM-DDTHH:mm:ss[Z]');

    let data = {
      name: this.form.value.name,
      announceDate: this.form.value.announceDate,
      cpv: this.form.value.cpv,
      user_id: this.form.value.user_id,
      status: true,
      walletStatus: true,
    };

    this.api.create(data).subscribe(
      (res) => {
        console.log(res);
        this.form.reset();
        this.submitted = false;
        this.notifier.show({
          type: 'success',
          message: 'Treasure is created',
        });
      },
      (err) => {
        this.notifier.show({
          type: 'error',
          message: JSON.stringify(err),
        });
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }
}
