import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from '../treasures/list/list.component';
import { CreateComponent } from '../treasures/create/create.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    pathMatch: 'full',
    data: { title: 'Treasures' },
  },
  {
    path: 'create',
    component: CreateComponent,
    data: { title: 'Create Treasure' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TreasuresRoutingModule {}
