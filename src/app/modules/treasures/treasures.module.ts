import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared.module';
import { TreasuresRoutingModule } from '../treasures/treasures-routing.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';

@NgModule({
  declarations: [ListComponent, CreateComponent],
  imports: [CommonModule, SharedModule, TreasuresRoutingModule],
})
export class TreasuresModule {}
