import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class TreasuresService {
  constructor(private http: HttpClient) {}

  get list() {
    return this.http.get(
      `${environment.apiBase}/treasure/list?limit=999&skip=0`,
      {
        headers: new HttpHeaders({
          Authorization: localStorage.token,
        }),
      }
    );
  }

  create(data) {
    return this.http.post(`${environment.apiBase}/treasure/add`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  get(id) {
    return this.http.get(`${environment.apiBase}/treasure/${id}`, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }

  update(id, data) {
    return this.http.put(`${environment.apiBase}/treasure/update/${id}`, data, {
      headers: new HttpHeaders({
        Authorization: localStorage.token,
      }),
    });
  }
}
