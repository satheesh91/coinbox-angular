import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NotifierModule } from 'angular-notifier';
import { MetismenuAngularModule } from '@metismenu/angular';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BlockUIModule } from 'ng-block-ui';
import { NgApexchartsModule } from 'ng-apexcharts';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NotifierModule,
    MetismenuAngularModule,
    BsDropdownModule,
    PaginationModule,
    TooltipModule,
    BlockUIModule,
    NgApexchartsModule,
    ModalModule,
    NgxMaskModule,
  ],
  exports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NotifierModule,
    MetismenuAngularModule,
    BsDropdownModule,
    PaginationModule,
    TooltipModule,
    BlockUIModule,
    NgApexchartsModule,
    ModalModule,
    NgxMaskModule,
  ],
})
export class SharedModule {}
