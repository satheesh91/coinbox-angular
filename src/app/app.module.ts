import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './_shared/_layouts/main-layout/main-layout.component';
import { HeaderComponent } from './_shared/_parts/header/header.component';
import { FooterComponent } from './_shared/_parts/footer/footer.component';
import { LeftbarComponent } from './_shared/_parts/leftbar/leftbar.component';
import { PlainLayoutComponent } from './_shared/_layouts/plain-layout/plain-layout.component';
import { AuthGuard } from './modules/auth/auth-gurad.service';
import { SharedModule } from './shared.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BlockUIModule } from 'ng-block-ui';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './modules/auth/token.interceptor';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    HeaderComponent,
    FooterComponent,
    PlainLayoutComponent,
    LeftbarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    BsDropdownModule.forRoot(),
    PaginationModule.forRoot(),
    TooltipModule.forRoot(),
    BlockUIModule.forRoot(),
    NgxMaskModule.forRoot(),
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
