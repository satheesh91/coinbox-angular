import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './modules/auth/auth-gurad.service';
import { MainLayoutComponent } from './_shared/_layouts/main-layout/main-layout.component';
import { PlainLayoutComponent } from './_shared/_layouts/plain-layout/plain-layout.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  {
    path: '',
    component: PlainLayoutComponent,
    children: [
      {
        path: 'auth',
        loadChildren: () =>
          import('./modules/auth/auth.module').then((m) => m.AuthModule),
      },
    ],
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./modules/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'users',
        loadChildren: () =>
          import('./modules/users/users.module').then((m) => m.UsersModule),
      },
      {
        path: 'requests',
        loadChildren: () =>
          import('./modules/withdraw-request/withdraw-request.module').then(
            (m) => m.WithdrawRequestModule
          ),
      },
      {
        path: 'news',
        loadChildren: () =>
          import('./modules/news/news.module').then((m) => m.NewsModule),
      },
      {
        path: 'treasures',
        loadChildren: () =>
          import('./modules/treasures/treasures.module').then(
            (m) => m.TreasuresModule
          ),
      },
      {
        path: 'files',
        loadChildren: () =>
          import('./modules/files/files.module').then((m) => m.FilesModule),
      },
      {
        path: 'my-values',
        loadChildren: () =>
          import('./modules/user-values/user-values.module').then(
            (m) => m.UserValuesModule
          ),
      },
      {
        path: 'downlines',
        loadChildren: () =>
          import('./modules/downlines/downlines.module').then(
            (m) => m.DownlinesModule
          ),
      },
      {
        path: 'payments',
        loadChildren: () =>
          import('./modules/payments/payments.module').then(
            (m) => m.PaymentsModule
          ),
      },
    ],
    canActivate: [AuthGuard],
  },
  { path: '**', redirectTo: '/dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
